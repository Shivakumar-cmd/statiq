import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
  } from 'react-native-responsive-screen';

  export const SIZES = {
    hp,
    wp,

  };
  export const COLORS = {
black:"rgba(0, 0, 0, 1)",
white:"#ffffff"

  };

const appTheme = { SIZES,COLORS};
export default appTheme;