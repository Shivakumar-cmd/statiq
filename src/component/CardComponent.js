import {StyleSheet, Text, View, Image} from 'react-native';
import React from 'react';
import {Card} from 'react-native-paper';
import {COLORS, SIZES} from '../constant/theme';
const CardComponent = ({data}) => {
  // source={{ uri: 'https://picsum.photos/700' }
  return (
    <View style={styles.cardContainer}>
      <Card style={styles.card}>
        <View style={styles.row}>
          <View
            style={{
              height: SIZES.hp(20),
              width: SIZES.wp(40),
              borderTopLeftRadius: SIZES.hp(1),
              borderBottomLeftRadius: SIZES.hp(1),
            }}>
            <Image source={{uri: data?.image}} style={styles.image} />
          </View>
          <View style={styles.nameContainer}>
            <Text
              style={styles.nameText}
              numberOfLines={2}
              ellipsizeMode="tail">
              {data?.name}
            </Text>
            <View style={styles.statusContainer}>
              <View
                style={[
                  styles.statusColor,
                  {backgroundColor: data?.status == 'Alive' ? 'green' : 'red'},
                ]}
              />
              <Text style={styles.text}>{data?.status}</Text>
            </View>
            <View style={styles.firstSeenContainer}>
              <Text style={styles.firstSeen}>First seen in:</Text>
              <Text style={styles.text} numberOfLines={2} ellipsizeMode="tail">
                {data?.result?.name}
              </Text>
            </View>
          </View>
        </View>
      </Card>
    </View>
  );
};

export default CardComponent;

const styles = StyleSheet.create({
  cardContainer: {paddingHorizontal: SIZES.hp(3)},
  card: {
    height: SIZES.hp(20),
    flex: 1,
    marginBottom: SIZES.hp(3),
    backgroundColor: COLORS.white,
    elevation: 1,
    shadowColor: COLORS.black,
  },
  row: {flexDirection: 'row'},
  text: {
    fontFamily: 'Inter-Regular',
    color: COLORS.black,
    fontSize: SIZES.hp(1.6),
  },
  image: {
    height: '100%',
    width: '100%',
    borderTopLeftRadius: SIZES.hp(1),
    borderBottomLeftRadius: SIZES.hp(1),
  },
  firstSeen: {
    fontFamily: 'Inter-Regular',
    color: 'rgba(139, 139, 139, 1)',
    fontSize: SIZES.hp(1.6),
  },
  firstSeenContainer: {flex: 1, justifyContent: 'flex-end'},
  statusColor: {
    height: SIZES.hp(1),
    width: SIZES.hp(1),
    borderRadius: SIZES.hp(1) / 2,

    marginRight: SIZES.hp(1),
  },
  statusContainer: {
    marginTop: SIZES.hp(1),
    flexDirection: 'row',
    alignItems: 'center',
  },
  nameContainer: {padding: SIZES.hp(1), flex: 1},
  nameText: {fontFamily: 'Inter-Bold', fontSize: SIZES.hp(1.8)},
});
