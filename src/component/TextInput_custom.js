import {StyleSheet, TextInput, View} from 'react-native';
import React from 'react';
import {COLORS, SIZES} from '../constant/theme';

const TextInput_custom = ({value, onChangeText}) => {
  return (
    <View>
      <TextInput
        style={styles.input}
        value={value}
        onChangeText={onChangeText}
        placeholder="search for your favourite character..."
        placeholderTextColor={COLORS.black}
      />
    </View>
  );
};

export default TextInput_custom;

const styles = StyleSheet.create({
  input: {
    height: SIZES.hp(8),
    borderRadius: SIZES.hp(1),
    borderColor: COLORS.black,
    borderWidth: SIZES.hp(0.2),
    paddingHorizontal: SIZES.hp(2),
    fontFamily: 'Inter-Regular',
  },
});
