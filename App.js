import React, {useCallback, useEffect, useState} from 'react';
import {StyleSheet, View, Text, FlatList} from 'react-native';
import {Provider as PaperProvider} from 'react-native-paper';
import CardComponent from './src/component/CardComponent';
import TextInput_custom from './src/component/TextInput_custom';
import {COLORS, SIZES} from './src/constant/theme';
import {useFonts} from 'expo-font';
import {SafeAreaView} from 'react-native-safe-area-context';
export default function App() {
  const [query, setQuery] = useState('');
  const [charcters, setAllCharacters] = useState([]);
  const [info, setInfo] = useState('');
  const [page, setPage] = useState(1);
  const [fontsLoaded] = useFonts({
    'Inter-Black': require('./src/assets/fonts/Inter-Black.ttf'),
    'Inter-Bold': require('./src/assets/fonts/Inter-Bold.ttf'),
    'Inter-Regular': require('./src/assets/fonts/Inter-Regular.ttf'),
  });
  const getEpisodes = async episode => {
    try {
      const response = await fetch(
        `https://rickandmortyapi.com/api/episode/${episode}`,
      );
      const json = await response.json();

      // setAllCharacters([...charcters,...json?.results]);
      // setInfo(json.info);
      return json;
    } catch (error) {
      console.error(error);
    } finally {
      // setLoading(false);
    }
  };
  const getMovies = async () => {
    let result;
    try {
      const response = await fetch(
        `https://rickandmortyapi.com/api/character?page=${page}`,
      );
      const json = await response.json();
      let data = [...json?.results];

      for (let i = 0; i < data.length; i++) {
        result = await getEpisodes(data[i].id);
        data[i].result = result;
      }
      setAllCharacters([...charcters, ...data]);

      setInfo(json.info);
    } catch (error) {
      console.error(error);
    } finally {
      // setLoading(false);
    }
  };

  useEffect(() => {
    getMovies();
  }, [page]);
  const onLayoutRootView = useCallback(async () => {
    if (fontsLoaded) {
      // await SplashScreen.hideAsync();
    }
  }, [fontsLoaded]);

  if (!fontsLoaded) {
    return null;
  }
  const onChangeText = q => {
    setQuery(q);

    const element = q.toLowerCase();
    const newUser = charcters.filter(user =>
      user.name.toLowerCase().includes(element),
    );

    setAllCharacters(newUser);
  };
  const onEndReached = () => {
    if (info?.next !== null) {
      setPage(page + 1);
    }
  };
  const renderItem = ({item, index}) => {
    return <CardComponent data={item} />;
  };

  return (
    <>
      <PaperProvider>
        <SafeAreaView style={[styles.container]} onLayout={onLayoutRootView}>
          <View style={styles.center}>
            <Text style={styles.appName}>Rick and Morty</Text>
          </View>
          <View style={{marginTop: SIZES.hp(3), marginHorizontal: SIZES.hp(3)}}>
            <TextInput_custom value={query} onChangeText={onChangeText} />
          </View>
          <View style={styles.countContainer}>
            <Text style={styles.count}>{info?.count} characters found</Text>
          </View>
          <View style={styles.flex}>
            <FlatList
              renderItem={renderItem}
              data={charcters}
              keyExtractor={item => item.id}
              onEndReached={onEndReached}
              onEndReachedThreshold={10}
            />
          </View>
        </SafeAreaView>
      </PaperProvider>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  center: {alignItems: 'center'},
  count: {
    fontFamily: 'Inter-Bold',
    fontSize: SIZES.hp(1.8),
    color: COLORS.black,
  },
  countContainer: {
    marginHorizontal: SIZES.hp(3),
    marginVertical: SIZES.hp(2),
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },
  flex: {flex: 1},
  appName: {
    fontSize: SIZES.hp(5.2),
    color: COLORS.black,
    fontFamily: 'Inter-Bold',
  },
});
